﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Music;

namespace MusicLogic
{
    class Duration
    {
        public static int CountLength(Disc disc)
        {
            int length=0;
            for (int i = 0; i < disc.ListOfSongs.Count; i++ )
            {
                length += disc.ListOfSongs[i].Length;
            }
            return length;
        }
    }
}
