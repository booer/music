﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Music;

namespace MusicLogic
{
    public class Factory
    {
        public static Pop CreatePop(string _name,int _year, string _singer,int _length, string _album, int _listenersnumber)
        {
            Pop pop = new Pop();
            pop.ListenersNumber = _listenersnumber;
            pop.Name = _name;
            pop.Year = _year;
            pop.Singer = _singer;
            pop.Length = _length;
            pop.Album = _album;
            return pop;
        }
        public static Rock CreateRock(string _name,int _year, string _singer,int _length, string _album, string _subgenre)
        {
            Rock rock = new Rock();
            rock.SubGenre = _subgenre;
            rock.Name = _name;
            rock.Year = _year;
            rock.Singer = _singer;
            rock.Length = _length;
            rock.Album = _album;
            return rock;
        }
        public static Dance CreateDance(string _name,int _year, string _singer,int _length, string _album, int _listenersnumber,int _temp)
        {
            Dance dance=new Dance();
            dance.Temp = _temp;
            dance.Name = _name;
            dance.Year = _year;
            dance.Singer = _singer;
            dance.Length = _length;
            dance.Album = _album;
            return dance;
        }
        public static SynthPop CreateSynthPop(string _name,int _year, string _singer,int _length, string _album, int _listenersnumber,string _label)
        {
            SynthPop synthpop = new SynthPop();
            synthpop.Label = _label;
            synthpop.Name = _name;
            synthpop.Year = _year;
            synthpop.Singer = _singer;
            synthpop.Length = _length;
            synthpop.Album = _album;
            return synthpop;
        }
        public static Disc CreateDisc()
        {
            Disc disc = new Disc();
            Pop track1 = CreatePop("Алые Розы",2007,"Антон Лебедев",900,"Зелёный альбом",32000);
            Rock track2 = CreateRock("Burn it down",2012,"Linkin Park",890,"The Fire","альтернативный рок");
            Dance track3 = CreateDance("Work Hard", 2014,"David Guetta",560,"L'omlette",9000,145);
            SynthPop track4 = CreateSynthPop("Белые Розы",1987,"Ласковый Май",890,"Синий",900,"ТПУ Рекордс");
            disc.AddSong(track1);
            disc.AddSong(track2);
            disc.AddSong(track3);
            disc.AddSong(track4);
            return disc;
        }
    }
}
