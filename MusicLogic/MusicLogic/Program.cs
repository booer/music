﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Music;

namespace MusicLogic
{
    public class MusicPrinter
    {
        public void TrackPrint (AbstractTrack Track)
    {
        Console.WriteLine("Название=" + Track.Name);
    }
        public void YearPrint(AbstractTrack Track)
        {
            Console.WriteLine("Год выпуска=" + Track.Year);
        }
        public void SingerPrint(AbstractTrack Track)
        {
            Console.WriteLine("Исполнитель =" + Track.Singer);
        }
        public void AlbumPrint(AbstractTrack Track)
        {
            Console.WriteLine("Название альбома =" + Track.Album);
        }
        public void LengthPrint(AbstractTrack Track)
        {
            Console.WriteLine("Длина трека =" + Track.Length);
        }

        public void AllPrint(Disc disc)
        {
                        for (int i = 0; i < disc.ListOfSongs.Count; i++)
            {

               TrackPrint(disc.ListOfSongs[i]);
               YearPrint(disc.ListOfSongs[i]);
               SingerPrint(disc.ListOfSongs[i]);
               AlbumPrint(disc.ListOfSongs[i]);
               LengthPrint(disc.ListOfSongs[i]);
            }

        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Disc disc = Factory.CreateDisc();
            MusicPrinter md = new MusicPrinter();
            md.AllPrint(disc);
        }
    }
}
