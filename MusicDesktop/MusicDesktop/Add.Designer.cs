﻿namespace MusicDesktop
{
    partial class Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboMusicBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Cancel = new System.Windows.Forms.Button();
            this.OK = new System.Windows.Forms.Button();
            this.Label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LabelBox = new System.Windows.Forms.TextBox();
            this.TempBox = new System.Windows.Forms.TextBox();
            this.ListenersNumberBox = new System.Windows.Forms.TextBox();
            this.SubGenreBox = new System.Windows.Forms.TextBox();
            this.AlbumBox = new System.Windows.Forms.TextBox();
            this.LengthBox = new System.Windows.Forms.TextBox();
            this.SingerBox = new System.Windows.Forms.TextBox();
            this.YearBox = new System.Windows.Forms.TextBox();
            this.NameBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ComboMusicBox
            // 
            this.ComboMusicBox.FormattingEnabled = true;
            this.ComboMusicBox.Items.AddRange(new object[] {
            "Поп",
            "Рок",
            "Танцевальная музыка",
            "Синти-поп"});
            this.ComboMusicBox.Location = new System.Drawing.Point(137, 416);
            this.ComboMusicBox.Name = "ComboMusicBox";
            this.ComboMusicBox.Size = new System.Drawing.Size(176, 21);
            this.ComboMusicBox.TabIndex = 43;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(28, 425);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 13);
            this.label10.TabIndex = 42;
            this.label10.Text = "Жанр музыки";
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(348, 366);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(131, 37);
            this.Cancel.TabIndex = 41;
            this.Cancel.Text = "Отмена";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(348, 291);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(131, 37);
            this.OK.TabIndex = 40;
            this.OK.Text = "ОК";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(31, 390);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(39, 13);
            this.Label9.TabIndex = 39;
            this.Label9.Text = "Лейбл";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 346);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 38;
            this.label8.Text = "Темп";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 305);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 37;
            this.label7.Text = "Слушатели";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 262);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Поджанр";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 35;
            this.label5.Text = "Альбом";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 170);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "Длина";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Исполнитель";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Год";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Название трека";
            // 
            // LabelBox
            // 
            this.LabelBox.Location = new System.Drawing.Point(137, 383);
            this.LabelBox.Name = "LabelBox";
            this.LabelBox.Size = new System.Drawing.Size(176, 20);
            this.LabelBox.TabIndex = 30;
            // 
            // TempBox
            // 
            this.TempBox.Location = new System.Drawing.Point(137, 339);
            this.TempBox.Name = "TempBox";
            this.TempBox.Size = new System.Drawing.Size(176, 20);
            this.TempBox.TabIndex = 29;
            // 
            // ListenersNumberBox
            // 
            this.ListenersNumberBox.Location = new System.Drawing.Point(137, 299);
            this.ListenersNumberBox.Name = "ListenersNumberBox";
            this.ListenersNumberBox.Size = new System.Drawing.Size(176, 20);
            this.ListenersNumberBox.TabIndex = 28;
            // 
            // SubGenreBox
            // 
            this.SubGenreBox.Location = new System.Drawing.Point(137, 256);
            this.SubGenreBox.Name = "SubGenreBox";
            this.SubGenreBox.Size = new System.Drawing.Size(176, 20);
            this.SubGenreBox.TabIndex = 27;
            // 
            // AlbumBox
            // 
            this.AlbumBox.Location = new System.Drawing.Point(137, 211);
            this.AlbumBox.Name = "AlbumBox";
            this.AlbumBox.Size = new System.Drawing.Size(176, 20);
            this.AlbumBox.TabIndex = 26;
            // 
            // LengthBox
            // 
            this.LengthBox.Location = new System.Drawing.Point(137, 164);
            this.LengthBox.Name = "LengthBox";
            this.LengthBox.Size = new System.Drawing.Size(176, 20);
            this.LengthBox.TabIndex = 25;
            // 
            // SingerBox
            // 
            this.SingerBox.Location = new System.Drawing.Point(137, 120);
            this.SingerBox.Name = "SingerBox";
            this.SingerBox.Size = new System.Drawing.Size(176, 20);
            this.SingerBox.TabIndex = 24;
            // 
            // YearBox
            // 
            this.YearBox.Location = new System.Drawing.Point(137, 75);
            this.YearBox.Name = "YearBox";
            this.YearBox.Size = new System.Drawing.Size(176, 20);
            this.YearBox.TabIndex = 23;
            // 
            // NameBox
            // 
            this.NameBox.Location = new System.Drawing.Point(137, 29);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(176, 20);
            this.NameBox.TabIndex = 22;
            // 
            // Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 451);
            this.Controls.Add(this.ComboMusicBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LabelBox);
            this.Controls.Add(this.TempBox);
            this.Controls.Add(this.ListenersNumberBox);
            this.Controls.Add(this.SubGenreBox);
            this.Controls.Add(this.AlbumBox);
            this.Controls.Add(this.LengthBox);
            this.Controls.Add(this.SingerBox);
            this.Controls.Add(this.YearBox);
            this.Controls.Add(this.NameBox);
            this.Name = "Add";
            this.Text = "Add";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboMusicBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.Label Label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LabelBox;
        private System.Windows.Forms.TextBox TempBox;
        private System.Windows.Forms.TextBox ListenersNumberBox;
        private System.Windows.Forms.TextBox SubGenreBox;
        private System.Windows.Forms.TextBox AlbumBox;
        private System.Windows.Forms.TextBox LengthBox;
        private System.Windows.Forms.TextBox SingerBox;
        private System.Windows.Forms.TextBox YearBox;
        private System.Windows.Forms.TextBox NameBox;
    }
}