﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Music;
using MusicLogic;

namespace MusicDesktop
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            LengthLabelText = "0";
        }
        private Disc formdisc = Factory.CreateDisc();
        public Disc FormDisc
        {
            get { return formdisc; }
            set { formdisc = value; }
        }

        private string _lengthLabeltext;
        public string LengthLabelText
        {
            set
            {
                _lengthLabeltext = "Длительность треков " + value;
                LengthLabel.Text = _lengthLabeltext;
            }
        }



        private bool _createTrack;
        public bool CreateTrack
        {
            get { return _createTrack; }
        }


        private int _musicIndex;
        public int MusicIndex
        {
            get { return _musicIndex; }
        }

        private List<AbstractTrack> _musicListBoxData = new List<AbstractTrack>();
        public List<AbstractTrack> MusicListBoxData
        {
            get { return _musicListBoxData; }
            set
            {
                _musicListBoxData = value;
                MusicBox.DataSource = _musicListBoxData;
            }
        }

        private void Add_Click(object sender, EventArgs e)
        {
            _createTrack = true;
            Add AddForm = new Add();
            AddForm.Owner = this;
            AddForm.ShowDialog();
        }

        private void Edit_Click(object sender, EventArgs e)
        {
            if (MusicBox.SelectedIndex != -1)
            {
                _createTrack = false;
                int index = MusicBox.SelectedIndex;
                _musicIndex = index;

                if (formdisc.ListOfSongs[index].GetType().Name == "Pop")
                {
                    Add EditForm = new Add((Pop)formdisc.ListOfSongs[index]);
                    EditForm.Owner = this;
                    EditForm.ShowDialog();
                }
                else if (formdisc.ListOfSongs[index].GetType().Name == "Rock")
                {
                    Add EditForm = new Add((Rock)formdisc.ListOfSongs[index]);
                    EditForm.Owner = this;
                    EditForm.ShowDialog();
                }
                else if (formdisc.ListOfSongs[index].GetType().Name == "SynthPop")
                {
                    Add EditForm = new Add((SynthPop)formdisc.ListOfSongs[index]);
                    EditForm.Owner = this;
                    EditForm.ShowDialog();
                }
                else if (formdisc.ListOfSongs[index].GetType().Name == "Dance")
                {
                    Add EditForm = new Add((Dance)formdisc.ListOfSongs[index]);
                    EditForm.Owner = this;
                    EditForm.ShowDialog();
                }


            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            Duration dur = new Duration();
            if (MusicBox.SelectedIndex != -1)
            {
                int index = MusicBox.SelectedIndex;
                int num;

                formdisc.RemoveSong(FormDisc.ListOfSongs[index]);
                num = formdisc.ListOfSongs.Count;
                MusicListBoxData = formdisc.ListOfSongs.GetRange(0, num);
                //LengthLabelText = Convert.ToString(dur.CountLength(FormDisc));
            }
        }

    }
}
