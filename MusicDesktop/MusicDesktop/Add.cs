﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Music;
using MusicLogic;

namespace MusicDesktop
{
    public partial class Add : Form
    {
        public Add()
        {
            InitializeComponent();
        }
        private DialogResult _result;
        public DialogResult Result
        {
            get { return _result; }
            set { _result = value; }
        }
        //Конструктор для редактирования
        public Add(Pop track)
            : this()
        {

            if (track != null)
            {
                NameBox.Text = track.Name.ToString();
                YearBox.Text = track.Year.ToString();
                SingerBox.Text = track.Singer.ToString();
                LengthBox.Text = track.Length.ToString();
                AlbumBox.Text = track.Album.ToString();
                ListenersNumberBox.Text = track.ListenersNumber.ToString();
                ComboMusicBox.Text = "Поп";
            }
        }

        public Add(Rock track)
            : this()
        {
            if (track != null)
            {
                NameBox.Text = track.Name.ToString();
                YearBox.Text = track.Year.ToString();
                SingerBox.Text = track.Singer.ToString();
                LengthBox.Text = track.Length.ToString();
                AlbumBox.Text = track.Album.ToString();
                SubGenreBox.Text = track.SubGenre.ToString();
                ComboMusicBox.Text = "Рок";
            }
        }
        public Add(SynthPop track)
            : this()
        {
            if (track != null)
            {
                NameBox.Text = track.Name.ToString();
                YearBox.Text = track.Year.ToString();
                SingerBox.Text = track.Singer.ToString();
                LengthBox.Text = track.Length.ToString();
                AlbumBox.Text = track.Album.ToString();
                ListenersNumberBox.Text = track.ListenersNumber.ToString();
                Label9.Text = track.Label.ToString();
                ComboMusicBox.Text = "Синти-поп";
            }
        }
        public Add(Dance track)
            : this()
        {
            if (track != null)
            {
                NameBox.Text = track.Name.ToString();
                YearBox.Text = track.Year.ToString();
                SingerBox.Text = track.Singer.ToString();
                LengthBox.Text = track.Length.ToString();
                AlbumBox.Text = track.Album.ToString();
                TempBox.Text = track.Temp.ToString();
                ListenersNumberBox.Text = track.ListenersNumber.ToString();
                ComboMusicBox.Text = "Танцевальная музыка";
            }
        }

        private void OK_Click(object sender, EventArgs e)
        {
            string name;
            int year;
            string singer;
            int length;
            string album;
            string subgenre;
            int listenersnumber;
            int temp;
            string label;
            int num;



            if (ComboMusicBox.Text == "Поп" || ComboMusicBox.Text == "Рок" || ComboMusicBox.Text == "Синти-поп" || ComboMusicBox.Text == "Танцевальная музыка")
            {
                Main main = this.Owner as Main;

                if (ComboMusicBox.Text == "Поп")
                {
                    if ((NameBox.Text != "") && (YearBox.Text != "") && (SingerBox.Text != "") && (LengthBox.Text != "") && (AlbumBox.Text != "") && (ListenersNumberBox.Text != ""))
                    {
                        name = NameBox.Text;
                        year = Convert.ToInt32(YearBox.Text);
                        singer = SingerBox.Text;
                        length = Convert.ToInt32(LengthBox.Text);
                        album = AlbumBox.Text;
                        listenersnumber = Convert.ToInt32(ListenersNumberBox.Text);
                        if (main.CreateTrack == true)
                        {
                            main.FormDisc.AddSong(Factory.CreatePop(name, year, singer, length, album, listenersnumber));
                        }
                        else
                        {
                            main.FormDisc.ListOfSongs[main.MusicIndex] = Factory.CreatePop(name, year, singer, length, album, listenersnumber);
                        }

                    }
                }
                else if (ComboMusicBox.Text == "Рок")
                {
                    if ((NameBox.Text != "") && (YearBox.Text != "") && (SingerBox.Text != "") && (LengthBox.Text != "") && (AlbumBox.Text != "") && (SubGenreBox.Text != ""))
                    {
                        name = NameBox.Text;
                        year = Convert.ToInt32(YearBox.Text);
                        singer = SingerBox.Text;
                        length = Convert.ToInt32(LengthBox.Text);
                        album = AlbumBox.Text;
                        subgenre = SubGenreBox.Text;
                        if (main.CreateTrack == true)
                        {
                            main.FormDisc.AddSong(Factory.CreateRock(name, year, singer, length, album, subgenre));
                        }
                        else
                        {
                            main.FormDisc.ListOfSongs[main.MusicIndex] = Factory.CreateRock(name, year, singer, length, album, subgenre);
                        }
                    }
                }
                else if (ComboMusicBox.Text == "Танцевальная музыка")
                {
                    if ((NameBox.Text != "") && (YearBox.Text != "") && (SingerBox.Text != "") && (LengthBox.Text != "") && (AlbumBox.Text != "") && (ListenersNumberBox.Text != "") && (TempBox.Text != ""))
                    {
                        name = NameBox.Text;
                        year = Convert.ToInt32(YearBox.Text);
                        singer = SingerBox.Text;
                        length = Convert.ToInt32(LengthBox.Text);
                        album = AlbumBox.Text;
                        listenersnumber = Convert.ToInt32(ListenersNumberBox.Text);
                        temp = Convert.ToInt32(TempBox.Text);
                        if (main.CreateTrack == true)
                        {
                            main.FormDisc.AddSong(Factory.CreateDance(name, year, singer, length, album, listenersnumber, temp));
                        }
                        else
                        {
                            main.FormDisc.ListOfSongs[main.MusicIndex] = Factory.CreateDance(name, year, singer, length, album, listenersnumber, temp);
                        }
                    }
                }
                else if (ComboMusicBox.Text == "Синти-поп")
                {
                    if ((NameBox.Text != "") && (YearBox.Text != "") && (SingerBox.Text != "") && (LengthBox.Text != "") && (AlbumBox.Text != "") && (ListenersNumberBox.Text != "") && (Label9.Text != ""))
                    {
                        name = NameBox.Text;
                        year = Convert.ToInt32(YearBox.Text);
                        singer = SingerBox.Text;
                        length = Convert.ToInt32(LengthBox.Text);
                        album = AlbumBox.Text;
                        listenersnumber = Convert.ToInt32(ListenersNumberBox.Text);
                        label = LabelBox.Text;
                        if (main.CreateTrack == true)
                        {

                            main.FormDisc.AddSong(Factory.CreateSynthPop(name, year, singer, length, album, listenersnumber, label));
                        }
                        else
                        {
                            main.FormDisc.ListOfSongs[main.MusicIndex] = Factory.CreateSynthPop(name, year, singer, length, album, listenersnumber, label);
                        }
                    }
                }
                if (main != null)
                {
                    Duration duration = new Duration();
                    //main.LengthLabelText = duration.CountLength(main.FormDisc);
                    //Добавить трек в список 
                    num = main.FormDisc.ListOfSongs.Count;
                    main.MusicListBoxData = main.FormDisc.ListOfSongs.GetRange(0, num);

                }
            }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            LengthBox.Text = "";
            NameBox.Text = "";
            YearBox.Text = "";
            SingerBox.Text = "";
            AlbumBox.Text = "";
            SubGenreBox.Text = "";
            TempBox.Text = "";
            LabelBox.Text = "";
            ListenersNumberBox.Text = "";
        }
        private void ComboMusicBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ComboMusicBox.Text)
            {
                case "Рок":
                    NameBox.ReadOnly = false;
                    YearBox.ReadOnly = false;
                    SingerBox.ReadOnly = false;
                    LengthBox.ReadOnly = false;
                    AlbumBox.ReadOnly = false;
                    SubGenreBox.ReadOnly = false;
                    ListenersNumberBox.ReadOnly = true;
                    TempBox.ReadOnly = true;
                    LabelBox.ReadOnly = true;
                    break;
                case "Поп":
                    NameBox.ReadOnly = false;
                    YearBox.ReadOnly = false;
                    SingerBox.ReadOnly = false;
                    LengthBox.ReadOnly = false;
                    AlbumBox.ReadOnly = false;
                    ListenersNumberBox.ReadOnly = false;
                    SubGenreBox.ReadOnly = true;
                    TempBox.ReadOnly = true;
                    LabelBox.ReadOnly = true;
                    break;
                case "Синти-поп":
                    LabelBox.ReadOnly = false;
                    NameBox.ReadOnly = false;
                    YearBox.ReadOnly = false;
                    SingerBox.ReadOnly = false;
                    LengthBox.ReadOnly = false;
                    AlbumBox.ReadOnly = false;
                    ListenersNumberBox.ReadOnly = false;
                    SubGenreBox.ReadOnly = true;
                    TempBox.ReadOnly = true;
                    break;
                case "Танцевальная музыка":
                    TempBox.ReadOnly = false;
                    NameBox.ReadOnly = false;
                    YearBox.ReadOnly = false;
                    SingerBox.ReadOnly = false;
                    LengthBox.ReadOnly = false;
                    AlbumBox.ReadOnly = false;
                    ListenersNumberBox.ReadOnly = false;
                    SubGenreBox.ReadOnly = true;
                    LabelBox.ReadOnly = true;
                    break;
            }

        }
    }
}
